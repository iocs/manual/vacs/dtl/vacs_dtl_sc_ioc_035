#
# Module: essioc
#
require essioc

#
# Module: vac_ctrl_digitelqpce
#
require vac_ctrl_digitelqpce,1.5.0


#
# Setting STREAM_PROTOCOL_PATH
#
epicsEnvSet(STREAM_PROTOCOL_PATH, "${vac_ctrl_digitelqpce_DB}")


#
# Module: essioc
#
iocshLoad("${essioc_DIR}/common_config.iocsh")

#
# Device: DTL-030:Vac-VEPI-10001
# Module: vac_ctrl_digitelqpce
#
iocshLoad("${vac_ctrl_digitelqpce_DIR}/vac_ctrl_digitelqpce_moxa.iocsh", "DEVICENAME = DTL-030:Vac-VEPI-10001, IPADDR = moxa-vac-dtl-2.tn.esss.lu.se, PORT = 4003")

#
# Device: DTL-030:Vac-VPN-10000
# Module: vac_ctrl_digitelqpce
#
iocshLoad("${vac_ctrl_digitelqpce_DIR}/vac_pump_digitelqpce_vpi.iocsh", "DEVICENAME = DTL-030:Vac-VPN-10000, CHANNEL = 1, CONTROLLERNAME = DTL-030:Vac-VEPI-10001")

#
# Device: DTL-030:Vac-VPN-20000
# Module: vac_ctrl_digitelqpce
#
iocshLoad("${vac_ctrl_digitelqpce_DIR}/vac_pump_digitelqpce_vpi.iocsh", "DEVICENAME = DTL-030:Vac-VPN-20000, CHANNEL = 2, CONTROLLERNAME = DTL-030:Vac-VEPI-10001")

#
# Device: DTL-030:Vac-VPN-30000
# Module: vac_ctrl_digitelqpce
#
iocshLoad("${vac_ctrl_digitelqpce_DIR}/vac_pump_digitelqpce_vpi.iocsh", "DEVICENAME = DTL-030:Vac-VPN-30000, CHANNEL = 3, CONTROLLERNAME = DTL-030:Vac-VEPI-10001")

#
# Device: DTL-030:Vac-VPN-40000
# Module: vac_ctrl_digitelqpce
#
iocshLoad("${vac_ctrl_digitelqpce_DIR}/vac_pump_digitelqpce_vpi.iocsh", "DEVICENAME = DTL-030:Vac-VPN-40000, CHANNEL = 4, CONTROLLERNAME = DTL-030:Vac-VEPI-10001")
